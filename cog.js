/* global log */
/* eslint-disable class-methods-use-this */

/**
 * This is the pronghorn.json global section. it has to be marked with @name pronghornTitleBlock tag
 * @name pronghornTitleBlock
 * @pronghornId @itentialopensource/tmfapi
 * @title tmfapi
 * @export tmfapi
 * @type Application
 * @summary This is the app summary
 * @src cog.js
 * @encrypted false
 * @roles admin engineering support apiread apiwrite
 */

class tmfapi {
  constructor() {
    // example constructor information
    this.example = 'this is an example';
    log.info('This log event is inside the constructor');
    // this array is for the demo functions
    this.users = [];
  }


  /**
  * @summary basic test call example
  *
  * @function testApi
   * @param {getCallback} callback - a callback function to return the result
   */
  /* YOU CAN CHANGE THE PARAMETERS YOU TAKE IN HERE AND IN THE pronghorn.json FILE */
  testApi(callback) {
    const response = {
      success: 200,
      message: 'This endpoint /testApi is active'
    };
    return callback(response);
  }

  /**
  * @summary basic call for use with the provided frontend
  * adds the paylod object to the users array and returns a response
  *
  * @function addInformation
  * @param {Object} payload object to push into the users array
   * @param {getCallback} callback - a callback function to return the result
   */
  /* YOU CAN CHANGE THE PARAMETERS YOU TAKE IN HERE AND IN THE pronghorn.json FILE */
  addInformation(payload, callback) {
    this.users.push(payload);
    const response = {
      success: 200,
      message: 'This endpoint /addInformation is active. The users list has been updated.'
    };
    return callback(response);
  }

  /**
  * @summary basic call for use with the provided frontend
  * returns the users array in a response object
  *
  * @function getInformation
   * @param {getCallback} callback - a callback function to return the result
   */
  /* YOU CAN CHANGE THE PARAMETERS YOU TAKE IN HERE AND IN THE pronghorn.json FILE */
  getInformation(callback) {
    const response = {
      success: 200,
      message: 'This endpoint /getInformation is active',
      users: this.users
    };
    return callback(response);
  }

  /**
  * @summary basic call for use with the provided frontend
  * returns the users array in a response object
  *
  * @function serviceOrderGet
   * @param {getCallback} callback - a callback function to return the result
   */
  /* YOU CAN CHANGE THE PARAMETERS YOU TAKE IN HERE AND IN THE pronghorn.json FILE */
  serviceOrderGet(callback) {
    const response = {
      success: 200,
      message: 'This endpoint /serviceOrderGet is active',
      users: this.users
    };
    return callback(response);
  }

  /**
  * @summary app status api call
  *
  * @function demoStatus
   * @param {getCallback} callback - a callback function to return the result
   */
  /* YOU CAN CHANGE THE PARAMETERS YOU TAKE IN HERE AND IN THE pronghorn.json FILE */
  demoStatus(callback) {
    const response = {
      success: 200,
      message: 'TMF Catlayst Demo is operational',
      users: this.users
    };
    return callback(response);
  }

  /**
  * @summary app status api call
  *
  * @function jobList
   * @param {getCallback} callback - a callback function to return the result
   */
  /* YOU CAN CHANGE THE PARAMETERS YOU TAKE IN HERE AND IN THE pronghorn.json FILE */
  jobList(callback) {
    cogs.WorkFlowEngine.getJobList("complete", {}, (results, error) => {
        if (error) return callback(null, error)
        return callback(results)
    });
  }
}

module.exports = new tmfapi();

